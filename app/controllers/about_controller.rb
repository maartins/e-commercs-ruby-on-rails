class AboutController < ApplicationController
  layout 'about', :only => [:index, :show]

  def index

  	@page_title = 'About Emporium'
  end
end
